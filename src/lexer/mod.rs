mod scanner;
mod token;

use self::{
    scanner::{Kind, Scanner},
    token::Tok,
};

pub struct Lexer<'i> {
    input: &'i str,
    scanner: Scanner<'i>,
    semicolon: Option<Tok<'i>>,
}

impl<'i> Lexer<'i> {
    pub fn new(input: &'i str) -> Self {
        let scanner = Scanner::new(input);
        Self {
            input,
            scanner,
            semicolon: None,
        }
    }

    fn eval(&mut self, kind: Kind, raw: &'i str) -> Option<Tok<'i>> {
        let tok = match kind {
            Kind::Comment => return None,
            Kind::Whitespace => return None,
            Kind::Newline => self.semicolon.take()?,

            Kind::Ident => match Tok::parse_keyword(raw) {
                Tok::Invalid(_) => Tok::Ident(raw),
                tok => tok,
            },
            Kind::Int => Tok::parse_int(raw),
            Kind::Float => Tok::parse_float(raw),
            Kind::Imag => Tok::parse_imag(raw),
            Kind::Rune => Tok::parse_rune(raw),
            Kind::String => Tok::parse_string(raw),
            Kind::RawString => Tok::parse_raw_string(raw),

            Kind::Add => Tok::Add,
            Kind::Sub => Tok::Sub,
            Kind::Mul => Tok::Mul,
            Kind::Quo => Tok::Quo,
            Kind::Rem => Tok::Rem,

            Kind::And => Tok::And,
            Kind::Or => Tok::Or,
            Kind::Xor => Tok::Xor,
            Kind::Shl => Tok::Shl,
            Kind::Shr => Tok::Shr,
            Kind::AndNot => Tok::AndNot,

            Kind::AddAssign => Tok::AddAssign,
            Kind::SubAssign => Tok::SubAssign,
            Kind::MulAssign => Tok::MulAssign,
            Kind::QuoAssign => Tok::QuoAssign,
            Kind::RemAssign => Tok::RemAssign,
            Kind::AndAssign => Tok::AndAssign,
            Kind::OrAssign => Tok::OrAssign,
            Kind::XorAssign => Tok::XorAssign,
            Kind::ShlAssign => Tok::ShlAssign,
            Kind::ShrAssign => Tok::ShrAssign,
            Kind::AndNotAssign => Tok::AndNotAssign,

            Kind::LAnd => Tok::LAnd,
            Kind::LOr => Tok::LOr,
            Kind::Arrow => Tok::Arrow,
            Kind::Inc => Tok::Inc,
            Kind::Dec => Tok::Dec,

            Kind::Eq => Tok::Eq,
            Kind::NotEq => Tok::NotEq,
            Kind::Less => Tok::Less,
            Kind::Greater => Tok::Greater,
            Kind::LessEq => Tok::LessEq,
            Kind::GreaterEq => Tok::GreaterEq,
            Kind::Not => Tok::Not,

            Kind::Assign => Tok::Assign,
            Kind::Define => Tok::Define,
            Kind::Ellipsis => Tok::Ellipsis,

            Kind::LParen => Tok::LParen,
            Kind::RParen => Tok::RParen,
            Kind::LBracket => Tok::LBracket,
            Kind::RBracket => Tok::RBracket,
            Kind::LBrace => Tok::LBrace,
            Kind::RBrace => Tok::RBrace,
            Kind::Comma => Tok::Comma,
            Kind::Period => Tok::Period,
            Kind::Semicolon => Tok::Semicolon,
            Kind::Colon => Tok::Colon,

            Kind::Invalid => Tok::Invalid(raw),
        };

        self.set_semicolon(&tok);
        Some(tok)
    }

    fn set_semicolon(&mut self, tok: &Tok<'i>) {
        match tok {
            Tok::Ident(_)
            | Tok::Int(_)
            | Tok::Float(_)
            | Tok::Imag(_)
            | Tok::Rune(_)
            | Tok::String(_)
            | Tok::Break
            | Tok::Continue
            | Tok::Fallthrough
            | Tok::Return
            | Tok::Inc
            | Tok::Dec
            | Tok::RParen
            | Tok::RBracket
            | Tok::RBrace => self.semicolon = Some(Tok::Semicolon),
            _ => self.semicolon = None,
        }
    }

    fn scan(&mut self) -> Option<(Kind, &'i str)> {
        let (kind, size) = self.scanner.next()?;
        let (raw, rest) = self.input.split_at(size);
        self.input = rest;
        Some((kind, raw))
    }
}

impl<'i> Iterator for Lexer<'i> {
    type Item = Tok<'i>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let (kind, raw) = match self.scan() {
                Some(s) => s,
                None => return self.semicolon.take(), // EOF
            };

            if let Some(tok) = self.eval(kind, raw) {
                return Some(tok);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rug::Float;

    fn lex(input: &str) -> Vec<Tok<'_>> {
        let lexer = Lexer::new(input);
        lexer.collect()
    }

    fn float(raw: &str) -> Float {
        let val = Float::parse(raw).unwrap();
        Float::with_val(256, val)
    }

    macro_rules! test_lex {
        ( $( $name:ident: $input:expr => $result:expr, )* ) => {
            $(
                #[test]
                fn $name() {
                    assert_eq!(lex($input)[0], $result);
                }
            )*
        };
    }

    test_lex! {
        ident_1: "foobar"  => Tok::Ident("foobar"),
        ident_2: "a۰۱۸"    => Tok::Ident("a۰۱۸"),
        ident_3: "foo६४"   => Tok::Ident("foo६४"),
        ident_4: "bar９８７６" => Tok::Ident("bar９８７６"),
        ident_5: "ŝ"       => Tok::Ident("ŝ"),
        ident_6: "ŝfoo"    => Tok::Ident("ŝfoo"),

        int_1: "0"                     => Tok::Int(0.into()),
        int_2: "1"                     => Tok::Int(1.into()),
        int_3: "123456789012345678890" => Tok::Int("123456789012345678890".parse().unwrap()),
        int_4: "01234567"              => Tok::Int(0o1234567.into()),
        int_5: "0xcafebabe"            => Tok::Int(0xcafebabe_u64.into()),
        int_6: "0XCAFEBABE"            => Tok::Int(0xcafebabe_u64.into()),

        float_1: "0."            => Tok::Float(float("0")),
        float_2: ".0"            => Tok::Float(float("0")),
        float_3: "3.14159265"    => Tok::Float(float("3.14159265")),
        float_4: "1e0"           => Tok::Float(float("1")),
        float_5: "1e+100"        => Tok::Float(float("1e+100")),
        float_6: "1E-100"        => Tok::Float(float("1e-100")),
        float_7: "2.71828e-1000" => Tok::Float(float("2.71828e-1000")),

        imag_1:  "0i"                     => Tok::Imag(float("0")),
        imag_2:  "1i"                     => Tok::Imag(float("1")),
        imag_3:  "012345678901234567889i" => Tok::Imag(float("12345678901234567889")),
        imag_4:  "123456789012345678890i" => Tok::Imag(float("123456789012345678890")),
        imag_5:  "0.i"                    => Tok::Imag(float("0")),
        imag_6:  ".0i"                    => Tok::Imag(float("0")),
        imag_7:  "3.14159265i"            => Tok::Imag(float("3.14159265")),
        imag_8:  "1e0i"                   => Tok::Imag(float("1")),
        imag_9:  "1e+100i"                => Tok::Imag(float("1e+100")),
        imag_10: "1e-100i"                => Tok::Imag(float("1e-100")),
        imag_11: "2.71828e-1000i"         => Tok::Imag(float("2.71828e-1000")),

        rune_1: r"'a'"          => Tok::Rune('a'),
        rune_2: r"'\000'"       => Tok::Rune('\0'),
        rune_3: r"'\xFF'"       => Tok::Rune('\u{ff}'),
        rune_4: r"'\uff16'"     => Tok::Rune('\u{ff16}'),
        rune_5: r"'\U0000ff16'" => Tok::Rune('\u{ff16}'),
        rune_6: r"'\''"         => Tok::Rune('\''),

        string_1: r#""foobar""#     => Tok::String("foobar".into()),
        string_2: r#""foo\nbar""#   => Tok::String("foo\nbar".into()),
        string_3: r#""foo\"bar""#   => Tok::String("foo\"bar".into()),
        string_4: r#""\000""#       => Tok::String("\0".into()),
        string_5: r#""\xFF""#       => Tok::String("\u{ff}".into()),
        string_6: r#""\uff16""#     => Tok::String("\u{ff16}".into()),
        string_7: r#""\U0000ff16""# => Tok::String("\u{ff16}".into()),

        raw_string_1: "`foobar`"     => Tok::String("foobar".into()),
        raw_string_2: "`foo
                        bar`"        => Tok::String("foo\n                        bar".into()),
        raw_string_3: "`\r`"         => Tok::String("".into()),
        raw_string_4: "`foo\r\nbar`" => Tok::String("foo\nbar".into()),

        kw_break:       "break"       => Tok::Break,
        kw_case:        "case"        => Tok::Case,
        kw_chan:        "chan"        => Tok::Chan,
        kw_const:       "const"       => Tok::Const,
        kw_continue:    "continue"    => Tok::Continue,
        kw_default:     "default"     => Tok::Default,
        kw_defer:       "defer"       => Tok::Defer,
        kw_else:        "else"        => Tok::Else,
        kw_fallthrough: "fallthrough" => Tok::Fallthrough,
        kw_for:         "for"         => Tok::For,
        kw_func:        "func"        => Tok::Func,
        kw_go:          "go"          => Tok::Go,
        kw_goto:        "goto"        => Tok::Goto,
        kw_if:          "if"          => Tok::If,
        kw_import:      "import"      => Tok::Import,
        kw_interface:   "interface"   => Tok::Interface,
        kw_map:         "map"         => Tok::Map,
        kw_package:     "package"     => Tok::Package,
        kw_range:       "range"       => Tok::Range,
        kw_return:      "return"      => Tok::Return,
        kw_select:      "select"      => Tok::Select,
        kw_struct:      "struct"      => Tok::Struct,
        kw_switch:      "switch"      => Tok::Switch,
        kw_type:        "type"        => Tok::Type,
        kw_var:         "var"         => Tok::Var,

        add: "+" => Tok::Add,
        sub: "-" => Tok::Sub,
        mul: "*" => Tok::Mul,
        quo: "/" => Tok::Quo,
        rem: "%" => Tok::Rem,

        and:     "&"  => Tok::And,
        or:      "|"  => Tok::Or,
        xor:     "^"  => Tok::Xor,
        shl:     "<<" => Tok::Shl,
        shr:     ">>" => Tok::Shr,
        and_not: "&^" => Tok::AndNot,

        add_assign:     "+="  => Tok::AddAssign,
        sub_assign:     "-="  => Tok::SubAssign,
        mul_assign:     "*="  => Tok::MulAssign,
        quo_assign:     "/="  => Tok::QuoAssign,
        rem_assign:     "%="  => Tok::RemAssign,
        and_assign:     "&="  => Tok::AndAssign,
        or_assign:      "|="  => Tok::OrAssign,
        xor_assign:     "^="  => Tok::XorAssign,
        shl_assign:     "<<=" => Tok::ShlAssign,
        shr_assign:     ">>=" => Tok::ShrAssign,
        and_not_assign: "&^=" => Tok::AndNotAssign,

        land:  "&&" => Tok::LAnd,
        lor:   "||" => Tok::LOr,
        arrow: "<-" => Tok::Arrow,
        inc:   "++" => Tok::Inc,
        dec:   "--" => Tok::Dec,

        eq:         "==" => Tok::Eq,
        not_eq:     "!=" => Tok::NotEq,
        less:       "<"  => Tok::Less,
        greater:    ">"  => Tok::Greater,
        less_eq:    "<=" => Tok::LessEq,
        greater_eq: ">=" => Tok::GreaterEq,
        not:        "!"  => Tok::Not,

        assign:   "="   => Tok::Assign,
        define:   ":="  => Tok::Define,
        ellipsis: "..." => Tok::Ellipsis,

        lparen:    "(" => Tok::LParen,
        rparen:    ")" => Tok::RParen,
        lbracket:  "[" => Tok::LBracket,
        rbracket:  "]" => Tok::RBracket,
        lbrace:    "{" => Tok::LBrace,
        rbrace:    "}" => Tok::RBrace,
        comma:     "," => Tok::Comma,
        period:    "." => Tok::Period,
        semicolon: ";" => Tok::Semicolon,
        colon:     ":" => Tok::Colon,
    }

    macro_rules! test_invalid {
        ( $( $name:ident: $input:expr, )* ) => {
            $(
                #[test]
                fn $name() {
                    assert_eq!(lex($input)[0], Tok::Invalid($input));
                }
            )*
        };
    }

    test_invalid! {
        invalid_octal_digit: "09",
        double_quote_escape_in_rune: r#"'\"'"#,
        single_quote_escape_in_string: r#""\'""#,
        multiple_chars_in_rune: "'aa'",
        invalid_octal_escape: r"'\400'",
        invalid_unicode_escape: r"'\udfff'",
    }

    #[test]
    fn semicolon_after_ident() {
        let toks = lex("foobar\n");
        assert_eq!(toks, [Tok::Ident("foobar".into()), Tok::Semicolon]);
    }

    #[test]
    fn semicolon_after_multiline_comment() {
        let toks = lex("return /* comment\n */");
        assert_eq!(toks, [Tok::Return, Tok::Semicolon]);
    }

    #[test]
    fn semicolon_before_eof() {
        let toks = lex("'a'");
        assert_eq!(toks, [Tok::Rune('a'), Tok::Semicolon]);
    }
}
