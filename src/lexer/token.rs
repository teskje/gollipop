use rug::{float::ParseFloatError, Float, Integer};
use std::convert::TryInto;

#[derive(Debug, Clone, PartialEq)]
pub enum Tok<'i> {
    Ident(&'i str), // main
    Int(Integer),   // 12345
    Float(Float),   // 123.45
    Imag(Float),    // 123.45i
    Rune(char),     // 'a'
    String(String), // "abc"

    Add, // +
    Sub, // -
    Mul, // *
    Quo, // /
    Rem, // %

    And,    // &
    Or,     // |
    Xor,    // ^
    Shl,    // <<
    Shr,    // >>
    AndNot, // &^

    AddAssign,    // +=
    SubAssign,    // -=
    MulAssign,    // *=
    QuoAssign,    // /=
    RemAssign,    // %=
    AndAssign,    // &=
    OrAssign,     // |=
    XorAssign,    // ^=
    ShlAssign,    // <<=
    ShrAssign,    // >>=
    AndNotAssign, // &^=

    LAnd,  // &&
    LOr,   // ||
    Arrow, // <-
    Inc,   // ++
    Dec,   // --

    Eq,        // ==
    NotEq,     // !=
    Less,      // <
    Greater,   // >
    LessEq,    // <=
    GreaterEq, // >=
    Not,       // !

    Assign,   // =
    Define,   // :=
    Ellipsis, // ...

    LParen,    // (
    RParen,    // )
    LBracket,  // [
    RBracket,  // ]
    LBrace,    // {
    RBrace,    // }
    Comma,     // ,
    Period,    // .
    Semicolon, // ;
    Colon,     // :

    // keywords
    Break,
    Case,
    Chan,
    Const,
    Continue,
    Default,
    Defer,
    Else,
    Fallthrough,
    For,
    Func,
    Go,
    Goto,
    If,
    Import,
    Interface,
    Map,
    Package,
    Range,
    Return,
    Select,
    Struct,
    Switch,
    Type,
    Var,

    Invalid(&'i str),
}

impl<'i> Tok<'i> {
    pub(super) fn parse_int(raw: &'i str) -> Self {
        let (val, radix) = parse_int_radix(raw);
        match Integer::from_str_radix(val, radix) {
            Ok(i) => Tok::Int(i),
            Err(_) => Tok::Invalid(raw),
        }
    }

    pub(super) fn parse_float(raw: &'i str) -> Self {
        match parse_float(raw) {
            Ok(f) => Tok::Float(f),
            Err(_) => Tok::Invalid(raw),
        }
    }

    pub(super) fn parse_imag(raw: &'i str) -> Self {
        debug_assert!(raw.ends_with('i'));
        let val = &raw[..raw.len() - 1];

        match parse_float(val) {
            Ok(f) => Tok::Imag(f),
            Err(_) => Tok::Invalid(raw),
        }
    }

    pub(super) fn parse_rune(raw: &'i str) -> Self {
        debug_assert!(raw.starts_with('\'') && raw.ends_with('\''));
        let val = &raw[1..raw.len() - 1];

        let parsed = if val.starts_with('\\') {
            parse_rune_escape(&val[1..])
        } else {
            parse_rune_single(val)
        };

        match parsed {
            Some(c) => Tok::Rune(c),
            None => Tok::Invalid(raw),
        }
    }

    pub(super) fn parse_string(raw: &'i str) -> Self {
        debug_assert!(raw.starts_with('"') && raw.ends_with('"'));
        let val = &raw[1..raw.len() - 1];

        match unescape_string(val) {
            Some(s) => Tok::String(s),
            None => Tok::Invalid(raw),
        }
    }

    pub(super) fn parse_raw_string(raw: &'i str) -> Self {
        debug_assert!(raw.starts_with('`') && raw.ends_with('`'));
        let val = &raw[1..raw.len() - 1];

        let s: String = val.chars().filter(|&c| c != '\r').collect();
        Tok::String(s)
    }

    pub(super) fn parse_keyword(raw: &'i str) -> Self {
        match raw {
            "break" => Tok::Break,
            "case" => Tok::Case,
            "chan" => Tok::Chan,
            "const" => Tok::Const,
            "continue" => Tok::Continue,
            "default" => Tok::Default,
            "defer" => Tok::Defer,
            "else" => Tok::Else,
            "fallthrough" => Tok::Fallthrough,
            "for" => Tok::For,
            "func" => Tok::Func,
            "go" => Tok::Go,
            "goto" => Tok::Goto,
            "if" => Tok::If,
            "import" => Tok::Import,
            "interface" => Tok::Interface,
            "map" => Tok::Map,
            "package" => Tok::Package,
            "range" => Tok::Range,
            "return" => Tok::Return,
            "select" => Tok::Select,
            "struct" => Tok::Struct,
            "switch" => Tok::Switch,
            "type" => Tok::Type,
            "var" => Tok::Var,
            _ => Tok::Invalid(raw),
        }
    }
}

fn parse_int_radix(raw: &str) -> (&str, i32) {
    let mut chars = raw.chars();
    if let Some('0') = chars.next() {
        match chars.next() {
            Some('x') | Some('X') => return (&raw[2..], 16),
            Some(_) => return (&raw[1..], 8),
            None => (),
        }
    }
    (raw, 10)
}

fn parse_float(raw: &str) -> Result<Float, ParseFloatError> {
    let val = Float::parse(raw)?;
    let float = Float::with_val(256, val);
    Ok(float)
}

fn parse_rune_escape(mut s: &str) -> Option<char> {
    if s.chars().next()? == '"' {
        // double-quote escapes are only valid in strings
        return None;
    }
    let c = parse_escape(&mut s)?;
    if !s.is_empty() {
        // trailing characters found
        return None;
    }
    Some(c)
}

fn parse_rune_single(val: &str) -> Option<char> {
    let chars: Vec<_> = val.chars().take(2).collect();
    if chars.len() == 1 {
        Some(chars[0])
    } else {
        None
    }
}

fn unescape_string(mut s: &str) -> Option<String> {
    let mut unescaped = String::new();
    while let Some(mut c) = s.chars().next() {
        s = &s[c.len_utf8()..];
        if c == '\\' {
            if s.chars().next()? == '\'' {
                // single-quote escapes are only valid in runes
                return None;
            }
            c = parse_escape(&mut s)?;
        }
        unescaped.push(c);
    }

    Some(unescaped)
}

fn parse_escape(s: &mut &str) -> Option<char> {
    let first = s.chars().next()?;

    let c = match first {
        'a' => '\x07',
        'b' => '\x08',
        'f' => '\x0c',
        'n' => '\n',
        'r' => '\r',
        't' => '\t',
        'v' => '\x0b',
        '\\' => '\\',
        '\'' => '\'',
        '\"' => '"',
        'x' | 'u' | 'U' => return parse_hex_escape(s),
        '0'..='7' => return parse_oct_escape(s),
        _ => return None,
    };

    *s = &s[c.len_utf8()..];
    Some(c)
}

fn parse_hex_escape(s: &mut &str) -> Option<char> {
    let first = s.chars().next()?;
    let len = match first {
        'x' => 3,
        'u' => 5,
        'U' => 9,
        _ => return None,
    };

    let hex = &s.get(1..len)?;
    *s = &s[len..];

    let num = u32::from_str_radix(hex, 16).ok()?;
    num.try_into().ok()
}

fn parse_oct_escape(s: &mut &str) -> Option<char> {
    let len = 3;
    let oct = &s.get(..len)?;
    *s = &s[len..];

    let num = u32::from_str_radix(oct, 8).ok()?;
    if num > 255 {
        return None;
    }

    num.try_into().ok()
}
